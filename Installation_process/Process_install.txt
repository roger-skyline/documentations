########################  Install process  ######################

# On first VM

- gitlab installation (SVN)
Available on : gitlab.bicheron.com:8888

- Nagios installation (Monitor)
Available on : gitlab.bicheron.com/nagios

- Syslog system (Client -> server) : https://wiki.debian.org/DebianEdu/HowTo/syslog-ng

- Installation Mysql server on 2 VM

- Replication Mysql Master -> Slave

- Install 2 apache server on 2 VM (Workers)

- Install 1 Apache server and create loadbalacing virtual host for the 2 workers