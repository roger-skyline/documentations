############################  SEMAPHORE  #################################

# Web frontend for ansible
# https://github.com/ansible-semaphore/semaphore

# Don't forget to forward mysql port and semaphore port

# Install mariadb-server package
sudo apt install mariadb-server

# Chose release corresponding ot you system
https://github.com/ansible-semaphore/semaphore/releases

# Download semaphore and place it in /usr/bin
curl -L <url-previously-choosend> > /usr/bin/semaphore

# Run setup and configure application
/usr/bin/semaphore -setup

# Run semaphore as daemon (AS ROOT)
nohup /usr/bin/semaphore -config /path/to/semaphore_config.json &

# If error : ImportError: No module named ansible.constants
# In ansible repository
source hacking/env-setup
export PYTHONPATH=/path/to/ansible/lib

# Start semaphore at boot
vi /etc/rc.local

+ nohup /usr/bin/semaphore -config /home/bbicheron/playbook/semaphore_config.json &
