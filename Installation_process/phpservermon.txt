######################  PHPSERVERMON  ##########################
# Monitor tools code in PHP

# Mysql, apache2 and php 5.6 is needed for this tools
sudo apt install apache2 php5 php5-mysql mysql-server php5-curl

# Download last package at : http://www.phpservermonitor.org/download/

# Untar package download and place it in appropriate directory
tar -xf phpservermon-3.2.0.tar.gz
mv phpservermon-3.2.0 /home/<user>/

# Install package
cd /home/<user>/phpservermon-3.2.0
php install.php

# Create virtual host in apache2
echo "<VirtualHost git.slash16.local:80>
        ServerName git.slash16.local

        DocumentRoot /home/bbichero/phpservermon-3.2.0

        CustomLog /var/log/apache/phpservermon-access.log combined
        ErrorLog /var/log/apache/phpservermon-error.log
</VirtualHost>" > /etc/apache2/sites-available/phpservermon.conf

# Create new directory in /var/log for apache log
sudo mkdir /var/log/apache

# Active new virtual host
sudo a2ensite phpservermon
sudo service apache2 restart

# Don't forget to forward port 80 in gate VM