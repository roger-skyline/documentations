# Documentation project
## List all Documentation of Roget-skyline 2

## VM 1 : gateway.slash.local
- GATEWAY
- BACKUP 1

## VM 2 : dhcp.slash.local
- DHCP
- SSL
- VPN
- BACKUP 2

## VM 3 : dns.slash.local
- LDAP
- DNS
- BACKUP 3

## VM 4 : git.slash.local
- GOGS
- TASKBOARD
- LOAD BALANCING

## VM 5 : worker1.slash.local
- WORKER 1

## VM 6 : worker2.slash.local
- WORKER 2

## VM 7 : mastersql.slash.local
- MYSQL MASTER

## VM 8 : slavesql.slash.local
- MYSQL SLAVE

## VM 9 : mail.slash.local
- DOVECOT
- POSTFIX

## VM 10 : preprod.slash.local
- PREPROD WORKER
- PREPROD MYSQL
- ANSIBLE
- NAGIOS
